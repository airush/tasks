**yandex_pill_scrapper**

Скраппер препаратов по побочным эффектам(если есть ссылки) сайта "Яндекс здоровье".

**For linux Chromedriver**

1.  Необходимо скачать [chromedriver версии google-chrome](http://chromedriver.storage.googleapis.com/index.html)
2.  Перенести его в директорию */usr/bin*
3.  И дать права доступа 
`sudo chmod a+x chromedriver`

**Запуск создав Virtual environment**

```
python3 -m venv venv
source venv/bin/activate
pip install -r ./requirements.txt
python3 ./Main.py
```


