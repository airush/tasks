from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import ElementClickInterceptedException
import time
import numpy as np
from bs4 import BeautifulSoup


# Определение побочныъ эффектов, которые ещё не были и которые уже были
def __get_unique_and_copy_side_ef(a_pill_side_ef, b_pill_side_ef, a_pill_side_ef_links, b_pill_side_ef_links):
    unique_new_side_effects = []
    copy_side_effect_links = []
    for k, b_link in enumerate(b_pill_side_ef_links):
        if b_link in a_pill_side_ef_links:
            copy_side_effect_links.append(b_link)
        else:
            unique_new_side_effects.append(b_pill_side_ef[k])
            a_pill_side_ef.append(b_pill_side_ef[k])
            a_pill_side_ef_links.append(b_link)
    return unique_new_side_effects, copy_side_effect_links


# Добавление правого блока нулей размероности новых побочных эффектов
def __concat_new_side_ef_col_with_zeros(matrix, unique_new_side_effects):
    appending_row_length = len(matrix)
    appending_col_length = len(unique_new_side_effects)
    appending_matrix = np.zeros((appending_row_length, appending_col_length), dtype=bool)
    return np.append(matrix, appending_matrix, axis=1)


def construct_table_and_req_lists(category_link: str):
    print("Executing ", category_link)
    browser = webdriver.Chrome()

    browser.get(category_link)

    delay = 10  # Longest delay, or stop programs

    def check_exists_by_xpath():
        try:
            return WebDriverWait(browser, delay).until(EC.presence_of_element_located(
                (By.XPATH, '/html/body/div[1]/div/main/div/div[2]/div[1]/div[1]/div[2]/div/div[2]/button')))
        except TimeoutException:
            return False

    # Пока есть кнопка "Показать ещё" загружаем страницу
    while True:
        time.sleep(0.4)
        load_button = check_exists_by_xpath()
        if load_button:
            try:
                if load_button.tag_name == "button" and load_button.text == "Показать ещё":

                    load_button.click()
            except StaleElementReferenceException:
                break
            except ElementClickInterceptedException:
                break

        else:
            break

    soup = BeautifulSoup(browser.page_source, 'lxml')
    pill_cards = soup.find_all('div', class_="PillCardBoard-Item")
    print("Pill_cards_length = ", len(pill_cards))
    all_pills = []

    # Метод возращает имя препарата и его побочные эффекты
    def get_pill_info(index: int):
        pill_card = pill_cards[index].findChildren("a", recursive=False)
        pill_url = "https://yandex.ru" + pill_card[0].attrs['href']
        browser.get(pill_url)

        # Получаем название препарата с его дозировкой и типом
        title = browser.find_element_by_xpath("""//*[@id="root"]/div/main/div/div[2]/div/div/div[1]/h1""").text
        try:
            title += " " + browser.find_element_by_css_selector(
                "div.PillReleaseForm-Value > div > div > div.Dropdown-SelectedText").text
        except NoSuchElementException:
            title += " " + browser.find_elements_by_class_name("PillReleaseForm-Value_inline")[0].text
        print(str(index) + " is Index, Pill= " + title)

        # Переход на нужный div побочек, и ищем там ссылки
        try:
            side_effects_div_block = browser.find_element_by_xpath("//h3[@id='pobochnoe-dejstvie']/following-sibling::div")
        except NoSuchElementException:
            return None, None, None
        side_effects_link = side_effects_div_block.find_elements_by_tag_name('a')

        _pill_side_effects = []
        _pill_side_effects_links = []
        for i in range(len(side_effects_link)):
            pill_side_effect_link = side_effects_link[i].get_attribute('href')
            if pill_side_effect_link not in _pill_side_effects_links:
                _pill_side_effects.append(side_effects_link[i].text)
                _pill_side_effects_links.append(pill_side_effect_link)

        return title, _pill_side_effects, _pill_side_effects_links

    # Формирование таблицы с помощью первого элемента
    first_pill_title, side_effects, side_effects_links = get_pill_info(0)
    all_pills.append(first_pill_title)
    result_matrix = np.ones((1, len(side_effects)), dtype=bool)

    # Проходимся по всем карточкам, которые вытащили
    for i in range(len(pill_cards)):
        if i != 0:
            pill_title, pill_side_effects, pill_side_effects_links = get_pill_info(i)

            # Если прерарат с таким именем уже существует то пропускаем итерацию
            if pill_title is None or pill_title in all_pills:
                continue
            all_pills.append(pill_title)

            unique_new_side_effects, copy_side_effect_links = __get_unique_and_copy_side_ef(side_effects,
                                                                                            pill_side_effects,
                                                                                            side_effects_links,
                                                                                            pill_side_effects_links)

            result_matrix = __concat_new_side_ef_col_with_zeros(result_matrix, unique_new_side_effects)

            appending_bottom_row = np.ones(len(side_effects), dtype=bool)
            loop_until_index = -len(unique_new_side_effects) if (len(unique_new_side_effects) > 0) else len(
                side_effects)
            for l, side_ef_link in enumerate(side_effects_links[: loop_until_index]):
                if side_ef_link not in copy_side_effect_links:
                    appending_bottom_row[l] = False

            result_matrix = np.append(result_matrix, [appending_bottom_row], axis=0)

    browser.close()
    return result_matrix, all_pills, side_effects, side_effects_links


def concat_constructed_tables_and_req_lists(result_matrix, a_pill_names: list, a_pill_side_ef: list,
                                            a_pill_side_ef_links: list, b_matrix,
                                            b_pill_names: list, b_pill_side_ef: list, b_pill_side_ef_links: list):
    unique_new_side_effects, copy_side_effect_links = __get_unique_and_copy_side_ef(a_pill_side_ef,
                                                                                    b_pill_side_ef,
                                                                                    a_pill_side_ef_links,
                                                                                    b_pill_side_ef_links)
    result_matrix = __concat_new_side_ef_col_with_zeros(result_matrix, unique_new_side_effects)

    unique_b_pills = []
    for b_pill in b_pill_names:
        if b_pill not in a_pill_names:
            unique_b_pills.append(b_pill)
            a_pill_names.append(b_pill)

    for unique_b_pill in unique_b_pills:
        unique_b_pill_index_in_b_pills = b_pill_names.index(unique_b_pill)
        appending_bottom_row = np.zeros(len(a_pill_side_ef), dtype=bool)
        for k, sf_link in enumerate(a_pill_side_ef_links):
            b_sf_link_index = None
            try:
                b_sf_link_index = b_pill_side_ef_links.index(sf_link)
            except ValueError:
                pass
            if b_sf_link_index is not None:
                transit_var = b_matrix[unique_b_pill_index_in_b_pills][b_sf_link_index]
                appending_bottom_row[k] = transit_var

        result_matrix = np.append(result_matrix, [appending_bottom_row], axis=0)

    return result_matrix, a_pill_names, a_pill_side_ef, a_pill_side_ef_links
