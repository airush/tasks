from selenium import webdriver
import pandas as pd
from CategoryScrapper import construct_table_and_req_lists
from CategoryScrapper import concat_constructed_tables_and_req_lists
import operator

browser = webdriver.Chrome()

YANDEX_HEALTH_PILLS_CARDIOVASCULAR_SYSTEM = "https://yandex.ru/health/pills?category=serdechno-sosudistaya-sistema-c"
YANDEX_HEALTH_PILLS_ANTIMICROBIAL = "https://yandex.ru/health/pills?category=protivomikrobnye-preparaty-dlya-sistemnogo-primeneniya-j"


# Сохранение в xlsx файл
def __save_category_into_xlsx_file(matrix, pill_names: list, side_effects: list, file_name="output"):
    file_name += ".xlsx"
    mdict = {}
    for i, pill in enumerate(pill_names):
        mdict[pill] = matrix[i]

    df = pd.DataFrame.from_dict(mdict, orient='index', columns=side_effects)
    df.to_excel(file_name)


def __scrap_full_category(url_path: str):
    browser.get(url_path)
    full_category = browser.find_element_by_class_name("PillCategories-SubcategoryBoard")
    browser_sub_category_links = full_category.find_elements_by_tag_name("a")
    sub_category_links = []
    for browser_sub_category_link in browser_sub_category_links:
        sub_category_links.append(browser_sub_category_link.get_attribute("href"))

    matrix, pill_names, side_ef, side_ef_links = None, None, None, None
    for i, sub_category_link in enumerate(sub_category_links):
        if i == 0:
            matrix, pill_names, side_ef, side_ef_links = construct_table_and_req_lists(sub_category_link)
        else:
            add_matrix, add_pill_names, add_side_ef, add_side_ef_links = construct_table_and_req_lists(
                sub_category_link)
            matrix, pill_names, side_ef, side_ef_links = concat_constructed_tables_and_req_lists(matrix, pill_names,
                                                                                                 side_ef, side_ef_links,
                                                                                                 add_matrix,
                                                                                                 add_pill_names,
                                                                                                 add_side_ef,
                                                                                                 add_side_ef_links)

    return matrix, pill_names, side_ef, side_ef_links


# 2 Задание
def __get_descending_sorted_side_effects(matrix, side_effects: list):
    ascending_sorted_list = []
    for i, side_effect in enumerate(side_effects):
        side_effect_counter = 0
        for el in matrix[:, i]:
            if el:
                side_effect_counter += 1
        ascending_sorted_list.append((side_effect, side_effect_counter))
    ascending_sorted_list.sort(key=lambda x: x[1])
    ascending_sorted_list.reverse()
    return ascending_sorted_list


# 3 Задание

def __get_unique_side_effects(matrix, side_effects: list):
    unique_side_effects = []
    for i, side_effect in enumerate(side_effects):
        side_effect_counter = 0
        for el in matrix[:, i]:
            if el:
                side_effect_counter += 1
                if side_effect_counter > 1:
                    break
        if side_effect_counter != 1:
            continue
        else:
            unique_side_effects.append(side_effect)

    return unique_side_effects


def __save_list_into_txt_file(file_name, s_list: list):
    with open(file_name, 'w') as f:
        for item in s_list:
            f.write("%s\n" % str(item))

print("** CARDIOVASCULAR_SYSTEM\n\n")
card_sys_matrix, card_sys_pill_names, card_sys_side_ef, card_sys_side_ef_links = __scrap_full_category(
    YANDEX_HEALTH_PILLS_CARDIOVASCULAR_SYSTEM)
__save_category_into_xlsx_file(card_sys_matrix, card_sys_pill_names, card_sys_side_ef, "Card")

sorted_sf = __get_descending_sorted_side_effects(card_sys_matrix, card_sys_side_ef)
__save_list_into_txt_file("CARDIOVASCULAR_SYSTEM_SORTED_DESCENDING_SIDE_EFFECTS.txt", sorted_sf)

unique_side_effects = __get_unique_side_effects(card_sys_matrix, card_sys_side_ef)
__save_list_into_txt_file("CARDIOVASCULAR_SYSTEM_UNIQUE_SIDE_EFFECTS.txt", unique_side_effects)

print("\n\n** ANTIMICROBIAL\n\n")
antimicrobial_matrix, antimicrobial_pill_names, antimicrobial_side_ef, antimicrobial_side_ef_links = __scrap_full_category(
    YANDEX_HEALTH_PILLS_ANTIMICROBIAL)
browser.close()
__save_category_into_xlsx_file(antimicrobial_matrix, antimicrobial_pill_names, antimicrobial_side_ef, "ANTIMICROBIAL")

sorted_sf = __get_descending_sorted_side_effects(antimicrobial_matrix, antimicrobial_side_ef)
__save_list_into_txt_file("ANTIMICROBIAL_SORTED_DESCENDING_SIDE_EFFECTS.txt", sorted_sf)

unique_side_effects = __get_unique_side_effects(antimicrobial_matrix, antimicrobial_side_ef)
__save_list_into_txt_file("ANTIMICROBIALUNIQUE_SIDE_EFFECTS.txt", unique_side_effects)

combined_matrix, combined_pill_names, combined_side_ef, combined_side_ef_links = concat_constructed_tables_and_req_lists(
    card_sys_matrix, card_sys_pill_names, card_sys_side_ef, card_sys_side_ef_links,
    antimicrobial_matrix,
    antimicrobial_pill_names,
    antimicrobial_side_ef,
    antimicrobial_side_ef_links)
__save_category_into_xlsx_file(combined_matrix, combined_pill_names, combined_side_ef, "COMBINED")

sorted_sf = __get_descending_sorted_side_effects(combined_matrix, combined_side_ef)
__save_list_into_txt_file("COMBINED_SORTED_DESCENDING_SIDE_EFFECTS.txt", sorted_sf)

unique_side_effects = __get_unique_side_effects(combined_matrix, combined_side_ef)
__save_list_into_txt_file("COMBINED_SIDE_EFFECTS.txt", unique_side_effects)
