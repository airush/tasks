package helpers;

public class ArrayUtil {

    public static void print(int[] array){
        System.out.print("|");
        for (int j = 0; j < array.length; j++) {
                System.out.print(array[j] + " | ");
        }
    }

    public static void print(int[][] array){
        for (int i = 0; i < array.length; i++) {
            System.out.print(i + "- | ");
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " | ");
            }
            System.out.println();
        }
        System.out.println();
    }
}
