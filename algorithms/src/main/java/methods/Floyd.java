package methods;

import java.util.ArrayList;
import java.util.List;

public class Floyd {
    // Преобразованная матрица после алгоритма флоида
    private int[][] resultMatrix;

    // Матрица способов кратчайшего пути
    private int[][] matrixOfShortestPaths;

    public Floyd(){}

    /**
     * Конструктор сразу преобразовывает параметры в конечный результат алгоритма
     * @param matrix значения бесконечности должны быть -1
     */
    public Floyd(int[][] matrix){
        int size = matrix.length;
        // Инициализация матрицы куда нужно двигаться, изначально значение совпадает с колонкой
        matrixOfShortestPaths = new int[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (matrix[i][j] == -1 || matrix[i][j] ==0){
                    matrixOfShortestPaths[i][j]= -1;
                } else {
                    matrixOfShortestPaths[i][j]= j;
                }
            }
        }
        boolean haveToCheck = false; // Нужно поскольку тут используется не бесконечность а минус один
        for (int m = 0; m < size; m++) {
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    haveToCheck = (matrix[i][m] != -1 && matrix[m][j] != -1)? true : false;

                    if (haveToCheck && (matrix[i][j] > matrix[i][m] + matrix[m][j] || matrix[i][j] == -1 )){
                        matrix[i][j] = matrix[i][m] + matrix[m][j];
                        matrixOfShortestPaths[i][j] = matrixOfShortestPaths[i][m];
                    }
                }
            }
        }
        resultMatrix = matrix;
    }

    public int getMedian(){
        int minMedianLength = Integer.MAX_VALUE;
        int tempMinMedianLength = 0;
        int median = -1;
        for (int i = 0; i < resultMatrix.length; i++) {
            for (int j = 0; j < resultMatrix.length; j++) {
                if (resultMatrix[i][j]> 0){
                    tempMinMedianLength += resultMatrix[i][j];
                }
            }
            if (tempMinMedianLength < minMedianLength){
                minMedianLength = tempMinMedianLength;
                median = i;
            }
            tempMinMedianLength = 0;
        }
        return median;
    }

    public int getCenter(){
        int farthestVertexVal = Integer.MAX_VALUE;
        int tempMaximalMinLength = 0;
        int median = -1;
        for (int i = 0; i < resultMatrix.length; i++) {
            for (int j = 0; j < resultMatrix.length; j++) {
                if (resultMatrix[i][j]> tempMaximalMinLength){
                    tempMaximalMinLength = resultMatrix[i][j];
                }
            }
            if (tempMaximalMinLength < farthestVertexVal){
                farthestVertexVal = tempMaximalMinLength;
                median = i;
            }
            tempMaximalMinLength = 0;
        }
        return median;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("Матрица кратчайших путей.\n");

        for (int i = 0; i < resultMatrix.length; i++) {
            for (int j = 0; j < resultMatrix[i].length; j++) {
                stringBuilder.append(resultMatrix[i][j] + "; ");
            }
            stringBuilder.append(System.lineSeparator());
        }
        stringBuilder.append("\nМатрица способов добраться кратчайшим путем.\n");
        for (int i = 0; i < matrixOfShortestPaths.length; i++) {
            for (int j = 0; j < matrixOfShortestPaths[i].length; j++) {
                stringBuilder.append(matrixOfShortestPaths[i][j] + "; ");
            }
            stringBuilder.append(System.lineSeparator());
        }

        stringBuilder.append("Центр графа: ").append(getCenter()).append(System.lineSeparator());
        stringBuilder.append("Медиана графа: ").append(getCenter()).append(System.lineSeparator());
        return stringBuilder.toString();
    }

    public String printShortestPath(int vertexA, int vertexB){
        StringBuilder stringBuilder = new StringBuilder(String.valueOf(vertexA));

        while (matrixOfShortestPaths[vertexA][vertexB] != -1){
            stringBuilder.append(" -> ").append(matrixOfShortestPaths[vertexA][vertexB]);
            vertexA = matrixOfShortestPaths[vertexA][vertexB];
        }

        return stringBuilder.toString();
    }

    public int[] getShortestPath(int vertexA, int vertexB){
        List<Integer> path = new ArrayList<>();
        while (matrixOfShortestPaths[vertexA][vertexB] != -1){
            path.add(matrixOfShortestPaths[vertexA][vertexB]);
            vertexA = matrixOfShortestPaths[vertexA][vertexB];
        }

        return path.stream().mapToInt(i -> i).toArray();
    }

    public int getMinWeightForTwoVertices(int i, int j){
        return resultMatrix[i][j];
    }

    public int[][] getResultMatrix() {
        return resultMatrix;
    }

    public int[][] getMatrixOfShortestPaths() {
        return matrixOfShortestPaths;
    }
}
