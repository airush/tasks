package methods;

import java.util.Arrays;
import java.util.Stack;

public class Euler {

    /**
     * Метод может обрабатывать матрицы с циклами и кратными ребрами.
     * @param initMatrix матрица ориентированного графа. Значение по строкам указывает сколько выходящих дуг,
     * значение по столбцам кол-во входящих дуг
     * @return возвращает массив елементов в порядке ейлерового пути, иначе null.
     * Если есть ейлеров цикл ответ всегда будет начинаться с 0
     */
    public static int[] findEulerPathInOrientedGraph(int[][] initMatrix){
        int[][] matrix = copy2dArray(initMatrix);
        if (hasEulerCircuitCheckInOrientedGraph(matrix)){
            return findEulerCircuitInOrientedGraphWithoutCloneMatrix(0, matrix);
        } else if (hasEulerPathCheckInOrientedGraph(matrix)){
            // Нахождение вершины откуда нужно начать путь, и вершины где закончить путь
            int startVertex = -1;
            int endVertex = -1;
            for (int i = 0; i < matrix.length; i++) {
                int outArcCount = 0;
                int inArcCount = 0;
                for (int j = 0; j < matrix.length; j++) {
                    if (matrix[i][j] > 0)
                        outArcCount += matrix[i][j];
                    if (matrix[j][i] > 0)
                        inArcCount += matrix[j][i];
                }
                if (inArcCount > outArcCount)
                    endVertex = i;
                if (outArcCount > inArcCount)
                    startVertex = i;
            }
            // создание временной дуги и поиск ейлерового цикла
            matrix[endVertex][startVertex]++;
            int[] eulerCircuit = findEulerCircuitInOrientedGraphWithoutCloneMatrix(startVertex, matrix);

            // Убирается созданная ранее временная дуга, массив делится пополам, вторая часть является началом,
            // первая продолжением.
            // P.S. Желательно отрефакторить, и написать чтото нормально вместо трех циклов, сделаю это завтра :)
            int divisionIndex = -1;
            for (int i = 0; i < eulerCircuit.length-1; i++) {
                if (eulerCircuit[i] == endVertex && eulerCircuit[i+1] == startVertex){
                    divisionIndex = i;
                    break;
                }
            }
            int[] result = new int[eulerCircuit.length-1];
            int lastElem = 0;
            if (divisionIndex+1 < eulerCircuit.length){
                for (int i = 0; i < eulerCircuit.length-divisionIndex-2; i++) {
                    result[i] = eulerCircuit[divisionIndex+i+1];
                    lastElem++;
                }
            }
            for (int i = 0; i < divisionIndex+1; i++) {
                result[lastElem + i] =  eulerCircuit[i];
            }
            return result;
        } else
            return null;
    }

    public static int[] findEulerCircuitInOrientedGraphWithoutCloneMatrix(int startVertex, int[][] matrix){
        if (hasEulerCircuitCheckInOrientedGraph(matrix)){
            // Узнаем сколько всего ребер существует
            int edgesCount = 0;
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix.length; j++) {
                    if (matrix[i][j] > 0) {
                        edgesCount+=matrix[i][j];
                    }
                }
            }

            Stack<Integer> tempStack = new Stack<>();
            Stack<Integer> resultStack = new Stack<>();

            int currentV = startVertex;
            tempStack.push(currentV);
            while (edgesCount > 0) {
                // Находим ближайшую вершину куда будем идти,
                int newV = -1;
                for (int j = 0; j < matrix.length; j++) {
                    if (matrix[currentV][j] > 0) {
                        newV = j;
                        break;
                    }
                }
                // Если есть куда идти, удаляем ребро, переходим на новую вершину
                if (newV != -1) {
                    matrix[currentV][newV]--;
                    tempStack.push(newV);
                    currentV = newV;
                    edgesCount--;
                } else { // Если не было выходящих дуг, значит уперлись в тупик или у этой вершины нет выхода
                    // идем назад если стек больше 1(потомучто мы 1 элемент в самом начале уже положили)
                /*resultStack.push(tempStack.pop());
                currentV = tempStack.peek();
                */
                    if (tempStack.size()>1){
                        resultStack.push(tempStack.pop());
                        currentV = tempStack.peek();
                    } else {
                        tempStack.pop();
                        currentV++;
                        tempStack.push(currentV);
                    }
                }
            }
            // Скидываем все что было в изначальном стеке в результатирующий
            while (!tempStack.empty()) {
                resultStack.push(tempStack.pop());
            }
            int[] resultArray = new int[resultStack.size()];
            for (int i = 0; i < resultStack.size()+i; i++) {
                resultArray[i] = resultStack.pop();
            }
            return resultArray;
        }
        return null;
    }

    public static int[] findEulerCircuitInOrientedGraph(int[][] matrix){
        int[][] localMatrix = copy2dArray(matrix);
        return findEulerCircuitInOrientedGraphWithoutCloneMatrix(0, localMatrix);
    }

    /**
     * Ейлеров цикл в ориентировнном графе существует только тогда, когда кол-во входящих дуг равно исходящим
     */
    public static boolean hasEulerCircuitCheckInOrientedGraph(int[][] matrix){
        for (int i = 0; i < matrix.length; i++) {
            int outArcCount = 0;
            int inArcCount = 0;
            for (int j = 0; j < matrix.length; j++) {
                if (matrix[i][j] > 0)
                    outArcCount += matrix[i][j];
                if (matrix[j][i] > 0)
                    inArcCount += matrix[j][i];
            }
            if (outArcCount != inArcCount){
                return false;
            }
        }
        return true;
    }

    /**
     * Ейлеров путь в ориентированном графе сущ., только если вершин с кол-вом выходящих дуг большем входящих, и вершин с кол-вом входящих большем
     * выходящих дуг не больше одного
     */
    public static boolean hasEulerPathCheckInOrientedGraph(int[][] matrix){
        int totalOutArcCount = 0;
        int totalInArcCount = 0;
        for (int i = 0; i < matrix.length; i++) {
            int tempOutArcCount = 0;
            int tempInArcCount = 0;

            boolean notInSingleCycle = true;
            for (int j = 0; j < matrix.length; j++) {
                if (matrix[i][j] > 0) {
                    tempOutArcCount += matrix[i][j];
                    if (i != j)
                        notInSingleCycle = false;
                }
                if (matrix[j][i] > 0) {
                    tempInArcCount += matrix[j][i];
                    if (i != j)
                        notInSingleCycle = false;
                }
            }
            // Если вершина отделена от других и имеет цикл
            if (notInSingleCycle && tempInArcCount > 0 && tempOutArcCount > 0){
                totalInArcCount++;
                tempOutArcCount++;
            }
            if (tempOutArcCount != tempInArcCount){
                if (tempInArcCount > tempOutArcCount)
                    totalInArcCount++;
                else
                    totalOutArcCount++;
                if (totalInArcCount > 1  || totalOutArcCount > 1 )
                    return false;
            }
        }
        return totalInArcCount == totalOutArcCount;
    }

    private static int[][] copy2dArray(int[][] matrix) {
        return Arrays.stream(matrix)
                .map(a -> a.clone())
                .toArray(ints -> new int[ints][]);
    }




    /** --------------------------------------
     * Только для неориентированных графов
     */
    public static String findEulerCircuit(int[][] matrix){
        if (!hasEulerCircuit(matrix)){
            return "Нет эйлерового цикла";
        } else {
            Stack<Integer> eulerCircuit = getStackOfEulerCircuitPathByMatrix(matrix);
            StringBuilder strBuilder = new StringBuilder();
            strBuilder.append(eulerCircuit.pop());
            while (!eulerCircuit.empty()) {
                strBuilder.append(" - ").append(eulerCircuit.pop());
            }
            return strBuilder.toString();
        }
    }

    /**
     * Только для неориентированных графов
     */
    public static boolean hasEulerCircuit(int[][] matrix){
        boolean answer = true;
        for (int i = 0; i < matrix.length; i++) {
            int edgeCounter = 0;
            for (int j = 0; j < matrix.length; j++) {
                if (i != j && matrix[i][j] > 0)
                    edgeCounter++;
            }
            if (edgeCounter%2 == 1 )
                return false;
        }
        return answer;
    }

    private static Stack<Integer> getStackOfEulerCircuitPathByMatrix(int[][] matrix){
        return getStackOfEulerCircuitPathFromVertex(0, matrix);
    }
    /**
     * Только для неориентированных графов
     */
    private static Stack<Integer> getStackOfEulerCircuitPathFromVertex(int currentV, int [][] matrix){
        // Узнаем сколько всего ребер существует
        int edgesCount = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = i; j < matrix.length; j++) {
                if (matrix[i][j] > 0) {
                    edgesCount+=matrix[i][j];
                }
            }
        }

        Stack<Integer> tempStack = new Stack<>();
        Stack<Integer> resultStack = new Stack<>();

        tempStack.push(currentV);
        while (edgesCount > 0) {
            int newV = -1;
            for (int j = 0; j < matrix.length; j++) {
                if (matrix[currentV][j] > 0) {
                    newV = j;
                    break;
                }
            }
            if (newV != -1) {
                if (currentV != newV){
                    matrix[currentV][newV]--;
                    matrix[newV][currentV]--;
                } else {
                    matrix[currentV][newV]--;
                }

                tempStack.push(newV);
                currentV = newV;
                edgesCount--;
            } else {
                int lastV = tempStack.pop();
                resultStack.push(lastV);
                currentV = tempStack.peek();
            }
        }
        while (!tempStack.empty()) {
            resultStack.push(tempStack.pop());
        }
        return resultStack;
    }
}
