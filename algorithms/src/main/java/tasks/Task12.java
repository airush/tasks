package tasks;

import methods.Euler;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class Task12 {

    private static class Trapeze{
        private int number, firstVertex, secondVertex;

        /**
         * @param number номер строкки трапеции в документе
         * @param firstVertex вершина начала дуги
         * @param secondVertex вершина конца дуги
         */
        public Trapeze(int number, int firstVertex, int secondVertex){
            this.number = number;
            this.firstVertex = firstVertex;
            this.secondVertex = secondVertex;
        }
    }

    public static void main(String[] args) {
        int[] result = findTrapezeRibbon("input/task12/5");

        if (result != null){
            for (int i = 0; i < result.length; i++) {
                if (i !=result.length-1){
                    System.out.print(result[i]);
                    System.out.print(" - ");
                }
                else {
                    System.out.println(result[i]);
                }
            }
        } else {
            System.out.println("Невозможно построить ленту из этих трапеций");
        }
    }

    public static int[] findTrapezeRibbon(String filepath){
        Path path = Paths.get(filepath);
        // 1 step  создание кучи елементов
        BidiMap<Integer, Integer> vertexes = new DualHashBidiMap<>();
        Set<Trapeze> trapezes = new HashSet<>();
        int lineNumber = 0;
        int zeroPosition = -1;
        try(BufferedReader reader = Files.newBufferedReader(path, Charset.forName("UTF-8"))){
            String currentLine = null;
            while((currentLine = reader.readLine()) != null){
                if (lineNumber == 0){ // Просто пропускаю первую строку с кол-вом трапеций, в данном решение не нужна
                    lineNumber++;
                } else {
                    String[] points = currentLine.split("\\s");
                    int firstV = Integer.parseInt(points[0]);
                    int secondV = Integer.parseInt(points[1]) - Integer.parseInt(points[2]);
                    vertexes.put(firstV, vertexes.getOrDefault(firstV,vertexes.size()));
                    vertexes.put(secondV, vertexes.getOrDefault(secondV,vertexes.size()));
                    if (firstV == 0){
                        zeroPosition = vertexes.get(firstV);
                    }
                    trapezes.add(new Trapeze(lineNumber, firstV, secondV));
                    lineNumber++;
                }
            }
        }catch(IOException ex){
            ex.printStackTrace();
        }
        if (zeroPosition != -1){
            int[][] matrix = new int[vertexes.size()][vertexes.size()];
            trapezes.forEach(trapeze -> matrix[vertexes.get(trapeze.firstVertex)][vertexes.get(trapeze.secondVertex)]++);

            int[] eulerCircuit = Euler.findEulerCircuitInOrientedGraphWithoutCloneMatrix( zeroPosition, matrix);
            if (eulerCircuit == null)
                return null;
            int[] resultArray = new int[trapezes.size()];
            for (int i = 0; i < eulerCircuit.length-1; i++) {
                int first = vertexes.getKey(eulerCircuit[i]);
                int second = vertexes.getKey(eulerCircuit[i+1]);
                Trapeze trapeze = trapezes.stream().filter(o -> o.firstVertex == first && o.secondVertex == second).findFirst().get();
                resultArray[i] = trapeze.number;
                trapezes.remove(trapeze);
            }
            return resultArray;
        }
        return  null;
    }
}
