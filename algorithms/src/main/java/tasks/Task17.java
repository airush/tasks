package tasks;

import methods.Floyd;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;

public class Task17 {

    public static void main(String[] args) {
        System.out.println("The current working directory is " + System.getProperty("user.dir"));
        Scanner in = new Scanner(System.in);

        System.out.println("Укажите название файла, который содержит матрицу смежности: ");
        // String fileName = in.nextLine();
        String fileName = "input/task17.txt";
        Floyd floyd = getFloydFromFile(fileName);

        System.out.println(floyd);
        System.out.println("\n\t*** INFO ***\nЧтобы найти путь напишите 2 переменные через точку с запятой. e.g: 0;3;\n" +
                "Чтобы выбрать другой файл введите 'file <filename>'. \nЧтобы выйти 'exit' " );

        String line;
        while (true){
            try {

                line = in.nextLine();
                if ("exit".equals(line)){
                    System.exit(0);
                } else if (line.contains("file ")){
                    fileName = line.substring(5);
                    floyd = getFloydFromFile(fileName);
                    System.out.println("Файл считан.");
                    System.out.println(floyd);
                } else {
                    int[] vertexes = Arrays.stream(line.split(";")).mapToInt(Integer::parseInt).toArray();
                    System.out.println(floyd.printShortestPath(vertexes[0],vertexes[1]));
                }

            } catch (Exception e){
                System.err.println("Введите данные корректно!!!");
            }
        }
    }

    private static Floyd getFloydFromFile(String fileName){
        Floyd floyd = new Floyd();
        try(FileInputStream inputStream = new FileInputStream(fileName)){
            BufferedReader bRead = new BufferedReader(new InputStreamReader(inputStream));

            // Узнаем размерность мартицы смежности по колонкам
            bRead.mark(1000);
            int matrixLength = bRead.readLine().split("; ").length;
            int[][] initMatrix = new int[matrixLength][matrixLength];
            bRead.reset();

            String line;
            int i = 0; // iteration
            while ((line = bRead.readLine()) != null){
                int[] intArray = Arrays.stream(line.split("; "))
                        .mapToInt(str -> Integer.parseInt(str))
                        .toArray();

                for (int j = 0; j < matrixLength; j++) {
                    initMatrix[i][j] = intArray[j];
                }
                i++;
            }
            return new Floyd(initMatrix);
        } catch (IOException e){
            System.err.println("Чтото пошло не так. Ошибка с файлом!\n " + e.getMessage() );
        }
        return floyd;
    }
}
