package tasks;

import helpers.ArrayUtil;

/**
 * Задача о конвеерах. определить, какие рабочие места должны быть выбраны на первом конвейере,
 * а какие — на втором, чтобы минимизировать полное время, затраченное на заводе
 * на сборку одного автомобиля.
 */
public class Task4 {

    /**
     * @param a - массив длинны @param n -  рабочее место, строка - это номер конвеера
     * @param t - массив длинны @param n-1 - затраты на переход с одного конвеера на другой
     * @param e - массив из двух элементов - затраты старта шасси
     * @param x - массив из двух элементов - затраты выхода авто из шасси
     * @param n - кол-во рабочик мест, не очень понимаю зачем это нужно ведь можно просто взять length у @param a
     */
    public static int[] findAndPrintFastestStationPath(int[][] a, int[][]t, int[] e, int[] x , int n){
        int[][] f = new int[2][n]; // минимально возможное время, в течение которого шасси проходит отстартовой точки до рабочего места
        int[][] l = new int[2][n-1]; // номер конвейера, через п-е рабочее место которого проходит самый быстрый полный путь сборки.

        f[0][0] = e[0]+a[0][0];
        f[1][0] = e[1]+a[1][0];

        for (int j = 1; j < n; j++) {
            if (f[0][j-1] + a[0][j] <= f[1][j-1] + t[1][j-1] + a[0][j]){
                f[0][j] = f[0][j-1] + a[0][j];
                l[0][j-1] = 1;
            } else {
                f[0][j] = f[1][j-1] + t[1][j-1] + a[0][j];
                l[0][j-1] = 2;
            }
            if (f[1][j-1] + a[1][j] <= f[0][j-1] + t[0][j-1] + a[1][j]){
                f[1][j] = f[1][j-1] + a[1][j];
                l[1][j-1] = 2;
            } else {
                f[1][j] = f[0][j-1] + t[0][j-1] + a[1][j];
                l[1][j-1] = 1;
            }
        }

        int fLongestPath = -1;
        int lLastStation = -1;

        if (f[0][n-1] + x[0] <= f[1][n-1] + x[1]){
            fLongestPath = f[0][n-1] + x[0];
            lLastStation = 1;
        } else {
            fLongestPath = f[1][n-1] + x[1];
            lLastStation = 2;
        }

        ArrayUtil.print(f);
        ArrayUtil.print(l);
        System.out.println("F* = " + fLongestPath);
        System.out.println("L* = " + lLastStation);

        int[] resultStationPath = findStationPath(l, lLastStation);

        System.out.println("Путь станций по порядку в конвеере для производства машины за кратчайший срок:");
        ArrayUtil.print(resultStationPath);
        return findStationPath(l, lLastStation);
    }

    private static int[] findStationPath(int[][] l, int lastStation){
        int n = l[0].length;
        int[] conveyorPath = new int[n+1];

        conveyorPath[n] = lastStation;
        for (int i = n; i > 0; i--) {
            lastStation = l[lastStation-1][i-1];
            conveyorPath[i-1] = lastStation;
        }
        return conveyorPath;
    }
}
