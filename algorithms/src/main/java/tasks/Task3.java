package tasks;

/**
 * Найти число способов разделить n человек на k команд при любых n и k не больше 100
 * (алгоритм - число Стирлинга второго рода)
 * не использовать рекурсию, а использовать динамическое программирование
 */

public class Task3 {
    public static int combinationOfSplittingNPeopleOnKTeams(int n, int k){
        n++; // Размерность должна быть на один больше поскольку есть ещё 0
        int[][] stirlings = new int[n][n];

        for (int i = 0; i < n; i++) {
            stirlings[i][i] = 1;
        }

        for (int i = 2; i < n; i++) {
            for (int j = 1; j < i; j++) {
                stirlings[i][j] = stirlings[i-1][j-1]+ j * stirlings[i-1][j];
            }
        }
        return stirlings[n-1][k];
    }
}
