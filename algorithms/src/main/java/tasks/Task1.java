package tasks;

/**
 Есть массив на 99 млн элементов + 1. Все числа встречаются чётное число раз, кроме одного.
 Нужно найти это число за O(n).
 */
public class Task1 {
    public static int findSingleIntInArrayOfIntPairs(int[] array){
        int result = 0;
        for (int i = 0; i < array.length; i++) {
            result = result ^ array[i];
        }
        return result;
    }
}
