package tasks;

import methods.Euler;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Task15 {
    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("The current working directory is " + System.getProperty("user.dir"));
        Scanner in = new Scanner(System.in);

        System.out.println("Укажите название файла, который содержит города: ");
        //File file = new File(in.nextLine());
        File file = new File("input/task15.txt");

        in = new Scanner(file);

        int[][] alphabetMatrix = new int[26][26];
        String line;
        List<String> townList = new ArrayList<>();
        while (in.hasNextLine()){
            line = in.nextLine().toLowerCase();
            townList.add(line);
            int tI = line.charAt(0)-'a';
            int tJ = line.charAt(line.length()-1)-'a';
            alphabetMatrix[tI][tJ]++;
        }
        int[] path = Euler.findEulerPathInOrientedGraph(alphabetMatrix);
        if (path==null){
            System.out.println("Невозможно создать цепочку из данных городов!");
        } else {
            for (int i = 0; i < path.length - 1; i++) {
                char firstLetter = (char) (path[i] + 'a');
                char lastLetter = (char) (path[i + 1] + 'a');

                String town = townList.stream()
                        .filter(o -> o.charAt(0) == firstLetter && o.charAt(o.length() - 1) == lastLetter)
                        .findFirst()
                        .get();
                townList.remove(town);

                if (i != path.length - 2){
                    System.out.print(town);
                    System.out.print(" - ");
                }
                else
                    System.out.print(town);
            }
        }
    }
}
