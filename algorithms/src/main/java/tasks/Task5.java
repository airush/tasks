package tasks;

import java.util.ArrayList;
import java.util.List;

/**
 * Задача о дискретном рюкзаке
 */
public class Task5 {

    public static class Backpack{
        private int[] elements;
        private int maxValue;

        public Backpack(int[] elements, int maxValue) {
            this.elements = elements;
            this.maxValue = maxValue;
        }

        public int[] getElements() {
            return elements;
        }

        public int getMaxValue() {
            return maxValue;
        }
    }

    public static Backpack findWhatToPutInBackpack(int[] weight, int[] value, int backpackSpace){
        // Кол-во строк равно размеру рюкзака, ячейки раблицы это максимальная ценность i-го элемента
        // F(k,i) = max(v(i) + F(k-m, i-1), F(k,i-1))
        int[][] maxVal = new int[weight.length+1][backpackSpace+1];

        for (int i = 1; i < weight.length+1; i++) {
            for (int j = 1; j < backpackSpace+1; j++) {
                if (j - weight[i-1] < 0){
                    maxVal[i][j] = maxVal[i-1][j];
                } else {
                  maxVal[i][j] = Math.max(maxVal[i-1][j], value[i-1] + maxVal[i-1][j-weight[i-1]]);
                }
            }
        }

        List<Integer> elements = new ArrayList<>();

        int lastElement = weight.length;
        int column = backpackSpace;
        boolean untilFlag = true;
        while (untilFlag){
            if (maxVal[lastElement][column] != maxVal[lastElement-1][column]){
                elements.add(lastElement-1); // -1 потомучто, у нас нуделевой строки и столбца
                column = column - weight[lastElement-1];
            }
            lastElement--;
            if (lastElement == 0 || column ==0)
                untilFlag = false;
        }
        return new Backpack(elements.stream().mapToInt(i->i).toArray(), maxVal[weight.length][backpackSpace] );
    }
}
