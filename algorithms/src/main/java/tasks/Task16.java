package tasks;

import methods.Euler;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class Task16 {
    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("The current working directory is " + System.getProperty("user.dir"));
        Scanner in = new Scanner(System.in);

        System.out.println("Укажите название файла, который содержит доминошки: ");
        //File file = new File(in.nextLine());
        File file = new File("input/task16.txt");

        in = new Scanner(file);
        int dimension = in.nextInt();

        int[][] matrix = new int[dimension][dimension];

        String line;
        in.nextLine();
        while (in.hasNextLine()){
            line = in.nextLine();
            int[] vertexes = Arrays.stream(line.split(":")).mapToInt(Integer::parseInt).toArray();
            matrix[vertexes[0]][vertexes[1]] = 1;
            matrix[vertexes[1]][vertexes[0]] = 1;
        }

        System.out.println(Euler.findEulerCircuit(matrix));
    }
}
