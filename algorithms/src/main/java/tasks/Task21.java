package tasks;

import java.util.Random;

public class Task21 {

    private static final int NUMBER_OF_SEATS_IN_ROW = 100_000;
    private static final int ROWS = 500;
    /*
    NUMBER_OF_SEATS_IN_ROW = 10_000_000;
    ROWS = 20;
    Result  13.534146
    */
    /*
    NUMBER_OF_SEATS_IN_ROW = 50_000;
    ROWS = 500;
    Result  13.538944
    */
    /*
    NUMBER_OF_SEATS_IN_ROW = 100_000;
    ROWS = 500;
    Result  13.530764000000001
    */

    public static void main(String[] args) {
        Random random = new Random();

        int allSeatsQuantity = 0;
        int emptySeatsQuantity = 0;

        for (int row = 0; row < ROWS; row++) {
            System.out.println("Processing row: " +  row);
            boolean[] seats = new boolean[NUMBER_OF_SEATS_IN_ROW]; // Если true то место занято, по дефолту всё свободно то есть false

            for (int couple = 0; couple < NUMBER_OF_SEATS_IN_ROW * 20; couple++) {
                int randomSeat = random.nextInt(seats.length-1);
                if (!seats[randomSeat] && !seats[randomSeat+1]){
                    seats[randomSeat] = true;
                    seats[randomSeat+1] = true;
                }
            }
            int rowFreeSeatsCount = 0;
            for (int i = 0; i < seats.length - 1; i++) {
                if (!seats[i] && !seats[i+1]){
                    seats[i] = true;
                    seats[i+1] = true;
                }
            }
            for (int i = 0; i < seats.length; i++) {
                if (!seats[i])
                    rowFreeSeatsCount++;
            }
            allSeatsQuantity+= seats.length;
            emptySeatsQuantity+=rowFreeSeatsCount;
        }

        System.out.println("Доля свободных мест: " + ((double) emptySeatsQuantity/ (double) allSeatsQuantity) * 100);
    }
}
