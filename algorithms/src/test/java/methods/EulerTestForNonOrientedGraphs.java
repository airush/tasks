package methods;

import org.junit.Test;
import org.junit.Assert;

public class EulerTestForNonOrientedGraphs {

    @Test
    public void testEulerCircuit_Graph8x8_ShouldHaveEulerCircuit(){
        int[][] matrix = new int[][]{
                {0, 1, -1, 1, -1 , -1, -1, -1, -1},
                {1, 1, 1, -1, -1 ,-1, -1, 1, 1},
                {-1, 1, 0, 1, 1 ,-1, 1, -1, -1},
                {1, -1, 1, 0, -1, -1, -1, -1, -1},
                {-1, -1, 1, -1, 0, 1, -1, -1, -1},
                {-1, -1, -1, -1, 1, 0, 1, -1, -1},
                {-1, -1, 1, -1, -1, 1, 0, -1, -1},
                {-1, 1, -1, -1, -1, -1, -1, 1, 1},
                {-1, 1, -1, -1, -1, -1, -1, 1, 0}
        };
        Assert.assertEquals("0 - 1 - 1 - 7 - 7 - 8 - 1 - 2 - 4 - 5 - 6 - 2 - 3 - 0", Euler.findEulerCircuit(matrix));
    }

    @Test
    public void testHasEulerCircuit_Graph2x2_ShouldNotHaveEulerCircuit(){
        int[][] matrix = new int[][]{{1,1},{1, 0}};
        Assert.assertFalse(Euler.hasEulerCircuit(matrix));
    }

    @Test
    public void testHasEulerCircuit_Graph3x3_ShouldHaveEulerCircuit(){
        int[][] matrix = new int[][]{{1,1,1},{1, 0, 1 }, {1,1,0}};
        Assert.assertTrue(Euler.hasEulerCircuit(matrix));
    }

    @Test
    public void testHasEulerCircuit_Graph4x4_ShouldHaveEulerCircuit(){
        int[][] matrix = new int[][]{{0,1,0,1},{1, 1, 1 ,0}, {0,1,0,1},{1,0,1,0}};
        Assert.assertTrue(Euler.hasEulerCircuit(matrix));
    }

    @Test
    public void testHasEulerCircuit_GraphWithEulerPath_ShouldNotHaveEulerCircuit(){
        // This matrix has Euler path
        int[][] matrix = new int[][]{{0,1,0,1},{1, 1, 1 ,1}, {0,1,0,1},{1,1,1,0}};
        Assert.assertFalse(Euler.hasEulerCircuit(matrix));
    }
}
