package methods;

import org.junit.Assert;
import org.junit.Test;

public class FloydTest {
    private int[][] initial_matrix_one  =
                    {{0,1,2,-1},
                    {1,0,7,-1},
                    {2,7,0,3},
                    {-1,-1,3,0}};

    private int[][] initial_matrix_two  =
                    {{0,7,2,-1,-1,-1},
                    {7,0,1,-1,-1,-1},
                    {2,1,0,2,6,1},
                    {-1,-1,2,0,3,-1},
                    {-1,-1,6,3,0,5},
                    {-1,-1,1,-1,5,0}};
    @Test
    public void testResultMatrix1(){
        int[][] expected_matrix_one  =
                        {{0,1,2,5},
                        {1,0,3,6},
                        {2,3,0,3},
                        {5,6,3,0}};

        Floyd floyd = new Floyd(initial_matrix_one);

        Assert.assertArrayEquals("Первая матрица - ожидаемое значение не совпадает с действительным!", expected_matrix_one, floyd.getResultMatrix());
    }

    @Test
    public void testResultMatrix2(){
        int[][] expected_matrix_two =
                        {{0,3,2,4,7,3},
                        {3,0,1,3,6,2},
                        {2,1,0,2,5,1},
                        {4,3,2,0,3,3},
                        {7,6,5,3,0,5},
                        {3,2,1,3,5,0}};

        Floyd floyd = new Floyd(initial_matrix_two);
        System.out.println(floyd);
        Assert.assertArrayEquals("Вторая матрица - ожидаемое значение не совпадает с действительным!", expected_matrix_two, floyd.getResultMatrix());
    }

    @Test
    public void testMedian1(){
        Floyd floyd = new Floyd(initial_matrix_one);
        int actualMedian = floyd.getMedian();
        int expectedMedian = 0;

        Assert.assertEquals(expectedMedian, actualMedian);
    }

    @Test
    public void testMedian2(){
        Floyd floyd = new Floyd(initial_matrix_two);
        int actualMedian = floyd.getMedian();
        int expectedMedian = 2;

        Assert.assertEquals(expectedMedian, actualMedian);
    }

    @Test
    public void testCenter1(){
        Floyd floyd = new Floyd(initial_matrix_one);
        int actualCenter = floyd.getCenter();
        int expectedCenter = 2;

        Assert.assertEquals(expectedCenter, actualCenter);
    }

    @Test
    public void testCenter2(){
        Floyd floyd = new Floyd(initial_matrix_two);
        int actualCenter = floyd.getCenter();
        int expectedCenter = 3;

        Assert.assertEquals(expectedCenter, actualCenter);
    }

    @Test
    public void testShortestPathStringsForFirstMatrix(){
        Floyd floyd = new Floyd(initial_matrix_one);

        String shortestPathFromAtoD = floyd.printShortestPath(0, 3);
        Assert.assertEquals("0 -> 2 -> 3", shortestPathFromAtoD);
        String shortestPathFromDtoA = floyd.printShortestPath(3, 0);
        Assert.assertEquals("3 -> 2 -> 0", shortestPathFromDtoA);

        String shortestPathFromBtoD = floyd.printShortestPath(1, 3);
        Assert.assertEquals("1 -> 0 -> 2 -> 3", shortestPathFromBtoD);
        String shortestPathFromDtoB = floyd.printShortestPath(3, 1);
        Assert.assertEquals("3 -> 2 -> 0 -> 1", shortestPathFromDtoB);
    }

    @Test
    public void testShortestPathStringsForSecondMatrix(){
        Floyd floyd = new Floyd(initial_matrix_two);

        String shortestPathFromAtoE = floyd.printShortestPath(0, 4);
        Assert.assertEquals("0 -> 2 -> 3 -> 4", shortestPathFromAtoE);

        String shortestPathFromFtoB = floyd.printShortestPath(5, 1);
        Assert.assertEquals("5 -> 2 -> 1", shortestPathFromFtoB);
    }
}
