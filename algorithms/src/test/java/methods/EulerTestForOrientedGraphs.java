package methods;

import org.junit.Test;
import org.junit.Assert;

public class EulerTestForOrientedGraphs {
    private int[][] getMatrix1(){
        return new int[][]{{0, 1},{0,0}};
    }

    private int[][] getMatrix2(){
        return new int[][]{{0, 1, 1},{0,0, 0}, {0,0,0}};
    }

    private int[][] getMatrix3(){
        return new int[][]{{2}};
    }

    private int[][] getMatrix4(){
        return new int[][]{{0, 1},{1,0}};
    }

    private int[][] getMatrix5(){
        return new int[][]{{0, 1, 0},{0,0,1}, {0,0,0}};
    }

    private int[][] getMatrix6(){
        return new int[][]{{1, 0},{1,0}};
    }

    private int[][] getMatrix7(){
        return new int[][]{{1, 0},{1,1}};
    }

    private int[][] getMatrix8(){
        return new int[][]{{0, 1, 0},{0,0,1}, {1,0,0}};
    }

    private int[][] getMatrix9(){
        return new int[][]{{0, 1, 0},{0,0,1}, {2,0,0}};
    }

    private int[][] getMatrix10(){
        return new int[][]{{0, 0, 0,1},{0,0, 1,0}, {1,1,0,0}, {0,0,1,0}};
    }

    private int[][] getMatrix11(){
        return new int[][]{{0, 0, 0,1},{0,0, 1,0}, {1,0,0,0}, {0,0,1,0}};
    }

    private int[][] getMatrix12(){
        return new int[][]{{0, 0, 1,1,0},{1,0, 0,0, 0}, {0,1,1,0,1}, {1,0,1,0,0},{0,0,0,1,0}};
    }

    private int[][] getMatrix13(){
        return new int[][]{{0, 0, 1,1,0},{0,1, 0,0, 0}, {0,1,1,0,1}, {1,0,1,0,0},{0,0,0,1,0}};
    }

    private int[][] getMatrix14(){
        return new int[][]{{1, 0, 0},{0,1,1},{0,0,0}};
    }

    @Test
    public void findEulerPath_Matrix1(){
        int[] expected = {0, 1};
        Assert.assertArrayEquals(expected, Euler.findEulerPathInOrientedGraph(getMatrix1()));
    }

    @Test
    public void findEulerPath_Matrix2_ShouldReturnNull(){
        Assert.assertNull(Euler.findEulerPathInOrientedGraph(getMatrix2()));
    }

    @Test
    public void findEulerPath_Matrix3(){
        int[] expected = {0, 0, 0};
        Assert.assertArrayEquals(expected, Euler.findEulerPathInOrientedGraph(getMatrix3()));
    }

    @Test
    public void findEulerPath_Matrix4(){
        int[] expected = {0, 1, 0};
        Assert.assertArrayEquals(expected, Euler.findEulerPathInOrientedGraph(getMatrix4()));
    }

    @Test
    public void findEulerPath_Matrix5(){
        int[] expected = {0, 1, 2};
        Assert.assertArrayEquals(expected, Euler.findEulerPathInOrientedGraph(getMatrix5()));
    }

    @Test
    public void findEulerPath_Matrix6(){
        int[] expected = {1, 0, 0};
        Assert.assertArrayEquals(expected, Euler.findEulerPathInOrientedGraph(getMatrix6()));
    }

    @Test
    public void findEulerPath_Matrix7(){
        int[] expected = {1, 1, 0, 0};
        Assert.assertArrayEquals(expected, Euler.findEulerPathInOrientedGraph(getMatrix7()));
    }

    @Test
    public void findEulerPath_Matrix8(){
        int[] expected = {0, 1, 2, 0};
        Assert.assertArrayEquals(expected, Euler.findEulerPathInOrientedGraph(getMatrix8()));
    }

    @Test
    public void findEulerPath_Matrix9(){
        int[] expected = {2, 0, 1, 2, 0};
        Assert.assertArrayEquals(expected, Euler.findEulerPathInOrientedGraph(getMatrix9()));
    }

    @Test
    public void findEulerPath_Matrix10(){
        int[] expected = {0, 3, 2, 1, 2 ,0};
        Assert.assertArrayEquals(expected, Euler.findEulerPathInOrientedGraph(getMatrix10()));
    }

    @Test
    public void findEulerPath_Matrix11(){
        int[] expected = {1, 2, 0, 3, 2};
        Assert.assertArrayEquals(expected, Euler.findEulerPathInOrientedGraph(getMatrix11()));
    }

    @Test
    public void findEulerPath_Matrix12(){
        int[] expected = {0,2,1,0,3,2,2,4,3,0};
        Assert.assertArrayEquals(expected, Euler.findEulerPathInOrientedGraph(getMatrix12()));
    }

    @Test
    public void findEulerPath_Matrix13(){
        int[] expected = {0,3,2,2,4,3,0,2,1,1};
        Assert.assertArrayEquals(expected, Euler.findEulerPathInOrientedGraph(getMatrix13()));
    }

    @Test
    public void findEulerPath_Matrix14_ShouldReturnNull(){
        Assert.assertNull(Euler.findEulerPathInOrientedGraph(getMatrix14()));
    }

    @Test
    public void findEulerCircuit_matrix3(){
        int[] expected = {0, 0, 0};
        Assert.assertArrayEquals(expected, Euler.findEulerCircuitInOrientedGraph(getMatrix3()));
    }

    @Test
    public void findEulerCircuit_matrix4(){
        int[] expected = {0, 1, 0};
        Assert.assertArrayEquals(expected, Euler.findEulerCircuitInOrientedGraph(getMatrix4()));
    }

    @Test
    public void findEulerCircuit_matrix8(){
        int[] expected = {0, 1, 2, 0};
        Assert.assertArrayEquals(expected, Euler.findEulerCircuitInOrientedGraph(getMatrix8()));
    }

    @Test
    public void findEulerCircuit_matrix10(){
        int[] expected = {0, 3, 2, 1, 2 ,0};
        Assert.assertArrayEquals(expected, Euler.findEulerCircuitInOrientedGraph(getMatrix10()));
    }

    @Test
    public void findEulerCircuit_matrix12(){
        int[] expected = {0,2,1,0,3,2,2,4,3,0};
        Assert.assertArrayEquals(expected, Euler.findEulerCircuitInOrientedGraph(getMatrix12()));
    }

    @Test
    public void matrix1_HasEulerPath_Should(){
        Assert.assertTrue(Euler.hasEulerPathCheckInOrientedGraph(getMatrix1()));
    }

    @Test
    public void matrix2_HasEulerPath_ShouldNot(){
        Assert.assertFalse(Euler.hasEulerPathCheckInOrientedGraph(getMatrix2()));
    }

    @Test
    public void matrix3_HasEulerPath_Should(){
        Assert.assertTrue(Euler.hasEulerPathCheckInOrientedGraph(getMatrix3()));
    }

    @Test
    public void matrix4_HasEulerPath_Should(){
        Assert.assertTrue(Euler.hasEulerPathCheckInOrientedGraph(getMatrix4()));
    }

    @Test
    public void matrix5_HasEulerPath_Should(){
        Assert.assertTrue(Euler.hasEulerPathCheckInOrientedGraph(getMatrix5()));
    }

    @Test
    public void matrix6_HasEulerPath_Should(){
        Assert.assertTrue(Euler.hasEulerPathCheckInOrientedGraph(getMatrix6()));
    }

    @Test
    public void matrix7_HasEulerPath_Should(){
        Assert.assertTrue(Euler.hasEulerPathCheckInOrientedGraph(getMatrix7()));
    }

    @Test
    public void matrix8_HasEulerPath_Should(){
        Assert.assertTrue(Euler.hasEulerPathCheckInOrientedGraph(getMatrix8()));
    }

    @Test
    public void matrix9_HasEulerPath_Should(){
        Assert.assertTrue(Euler.hasEulerPathCheckInOrientedGraph(getMatrix9()));
    }

    @Test
    public void matrix10_HasEulerPath_Should(){
        Assert.assertTrue(Euler.hasEulerPathCheckInOrientedGraph(getMatrix10()));
    }

    @Test
    public void matrix11_HasEulerPath_Should(){
        Assert.assertTrue(Euler.hasEulerPathCheckInOrientedGraph(getMatrix11()));
    }

    @Test
    public void matrix12_HasEulerPath_Should(){
        Assert.assertTrue(Euler.hasEulerPathCheckInOrientedGraph(getMatrix12()));
    }

    @Test
    public void matrix13_HasEulerPath_Should(){
        Assert.assertTrue(Euler.hasEulerPathCheckInOrientedGraph(getMatrix13()));
    }

    @Test
    public void matrix14_HasEulerPath_ShouldNot(){
        Assert.assertFalse(Euler.hasEulerPathCheckInOrientedGraph(getMatrix14()));
    }

    @Test
    public void matrix1_HasEulerCircuit_ShouldNot(){
        Assert.assertFalse(Euler.hasEulerCircuitCheckInOrientedGraph(getMatrix1()));
    }

    @Test
    public void matrix2_HasEulerCircuit_ShouldNot(){
        Assert.assertFalse(Euler.hasEulerCircuitCheckInOrientedGraph(getMatrix2()));
    }

    @Test
    public void matrix3_HasEulerCircuit_Should(){
        Assert.assertTrue(Euler.hasEulerCircuitCheckInOrientedGraph(getMatrix3()));
    }

    @Test
    public void matrix4_HasEulerCircuit_Should(){
        Assert.assertTrue(Euler.hasEulerCircuitCheckInOrientedGraph(getMatrix4()));
    }

    @Test
    public void matrix5_HasEulerCircuit_ShouldNot(){
        Assert.assertFalse(Euler.hasEulerCircuitCheckInOrientedGraph(getMatrix5()));
    }

    @Test
    public void matrix6_HasEulerCircuit_ShouldNot(){
        Assert.assertFalse(Euler.hasEulerCircuitCheckInOrientedGraph(getMatrix6()));
    }

    @Test
    public void matrix7_HasEulerCircuit_ShouldNot(){
        Assert.assertFalse(Euler.hasEulerCircuitCheckInOrientedGraph(getMatrix7()));
    }

    @Test
    public void matrix8_HasEulerCircuit_Should(){
        Assert.assertTrue(Euler.hasEulerCircuitCheckInOrientedGraph(getMatrix8()));
    }

    @Test
    public void matrix9_HasEulerCircuit_ShouldNot(){
        Assert.assertFalse(Euler.hasEulerCircuitCheckInOrientedGraph(getMatrix9()));
    }

    @Test
    public void matrix10_HasEulerCircuit_Should(){
        Assert.assertTrue(Euler.hasEulerCircuitCheckInOrientedGraph(getMatrix10()));
    }

    @Test
    public void matrix11_HasEulerCircuit_ShouldNot(){
        Assert.assertFalse(Euler.hasEulerCircuitCheckInOrientedGraph(getMatrix11()));
    }

    @Test
    public void matrix12_HasEulerCircuit_Should(){
        Assert.assertTrue(Euler.hasEulerCircuitCheckInOrientedGraph(getMatrix12()));
    }

    @Test
    public void matrix13_HasEulerCircuit_ShouldNot(){
        Assert.assertFalse(Euler.hasEulerCircuitCheckInOrientedGraph(getMatrix13()));
    }
}
