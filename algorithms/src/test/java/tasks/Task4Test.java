package tasks;

import org.junit.Assert;
import org.junit.Test;

public class Task4Test {

    @Test
    public void printFastestWayToBuildCarTest1(){

        int[][] a = new int[][]{{7,9,3,4,8,4},{8,5,6,4,5,7}};
        int[][] t = new int[][]{{2,3,1,3,4},{2,1,2,2,1}};
        int[] e = new int[]{2,4};
        int[] x = new int[]{3,2};

        int[] result = Task4.findAndPrintFastestStationPath(a, t, e, x, a[0].length);
        int[] expectedPath = new int[]{1,2,1,2,2,1};
        Assert.assertArrayEquals(expectedPath, result);
    }
}
