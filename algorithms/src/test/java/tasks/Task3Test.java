package tasks;

import org.junit.Assert;
import org.junit.Test;

public class Task3Test {
    @Test
    public void splitNPeopleOnKTeams_1people1teams_shouldReturn1(){
        int people = 1;
        int teams = 1;

        int expectedPossibleCombinations = 1;
        Assert.assertEquals(expectedPossibleCombinations, Task3.combinationOfSplittingNPeopleOnKTeams(people, teams));
    }

    @Test
    public void splitNPeopleOnKTeams_2people1teams_shouldReturn1(){
        int people = 2;
        int teams = 1;

        int expectedPossibleCombinations = 1;
        Assert.assertEquals(expectedPossibleCombinations, Task3.combinationOfSplittingNPeopleOnKTeams(people, teams));
    }

    @Test
    public void splitNPeopleOnKTeams_2people2teams_shouldReturn1(){
        int people = 2;
        int teams = 2;

        int expectedPossibleCombinations = 1;
        Assert.assertEquals(expectedPossibleCombinations, Task3.combinationOfSplittingNPeopleOnKTeams(people, teams));
    }

    @Test
    public void splitNPeopleOnKTeams_3people1teams_shouldReturn1(){
        int people = 3;
        int teams = 1;

        int expectedPossibleCombinations = 1;
        Assert.assertEquals(expectedPossibleCombinations, Task3.combinationOfSplittingNPeopleOnKTeams(people, teams));
    }

    @Test
    public void splitNPeopleOnKTeams_3people2teams_shouldReturn3(){
        int people = 3;
        int teams = 2;

        int expectedPossibleCombinations = 3;
        Assert.assertEquals(expectedPossibleCombinations, Task3.combinationOfSplittingNPeopleOnKTeams(people, teams));
    }

    @Test
    public void splitNPeopleOnKTeams_4people2teams_shouldReturn7(){
        int people = 4;
        int teams = 2;

        int expectedPossibleCombinations = 7;
        Assert.assertEquals(expectedPossibleCombinations, Task3.combinationOfSplittingNPeopleOnKTeams(people, teams));
    }

    @Test
    public void splitNPeopleOnKTeams_7people3teams_shouldReturn301(){
        int people = 7;
        int teams = 3;

        int expectedPossibleCombinations = 301;
        Assert.assertEquals(expectedPossibleCombinations, Task3.combinationOfSplittingNPeopleOnKTeams(people, teams));
    }

    @Test
    public void splitNPeopleOnKTeams_9people4teams_shouldReturn67284(){
        int people = 9;
        int teams = 4;

        int expectedPossibleCombinations = 7770;
        Assert.assertEquals(expectedPossibleCombinations, Task3.combinationOfSplittingNPeopleOnKTeams(people, teams));
    }
}
