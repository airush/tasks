package tasks;

import org.junit.Assert;
import org.junit.Test;

public class Task5Test {

    @Test
    public void findWhatToPutInBackpackTest1(){
        int[] weight = new int[]{3, 4 ,4, 5};
        int[] value = new int[]{4, 5, 6, 6};
        int backpackSpace = 10;

        // максимальная возможная ценность 12: берем 4 и 3 элементы
        Task5.Backpack backpack = Task5.findWhatToPutInBackpack(weight, value, backpackSpace);

        // assert max value 12
        Assert.assertEquals(12, backpack.getMaxValue());

        // assert backpack elements(indexes)
        int[] elements = new int[]{3,2};
        Assert.assertArrayEquals(elements, backpack.getElements());
    }

    @Test
    public void findWhatToPutInBackpackTest2(){
        int[] weight = new int[]{2, 1 ,3, 2};
        int[] value = new int[]{12, 10, 20, 15};
        int backpackSpace = 5;

        // максимальная возможная ценность 5: берем 3,1,0 элементы
        Task5.Backpack backpack = Task5.findWhatToPutInBackpack(weight, value, backpackSpace);

        // assert max value 37
        Assert.assertEquals(37, backpack.getMaxValue());

        // assert backpack elements(indexes)
        int[] elements = new int[]{3,1,0};
        Assert.assertArrayEquals(elements, backpack.getElements());
    }
}
