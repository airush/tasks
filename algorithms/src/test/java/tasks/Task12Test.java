package tasks;

import org.junit.Assert;
import org.junit.Test;

public class Task12Test {

    private static final String INPUT_FOLDER = "src/test/input/test12";

    @Test
    public void findTrapezeRibbon_myInput1_shouldReturnNull() {
        int[] result = Task12.findTrapezeRibbon(INPUT_FOLDER + "/my1.txt");
        Assert.assertNull(result);
    }

    @Test
    public void findTrapezeRibbon_myInput2_shouldReturn21(){
        int[] result = Task12.findTrapezeRibbon(INPUT_FOLDER + "/my2.txt");
        int[] expected = new int[]{2,1};
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void findTrapezeRibbon_myInput3_shouldReturn312(){

        int[] result = Task12.findTrapezeRibbon(INPUT_FOLDER + "/my3.txt");
        int[] expected = new int[]{3,1,2};
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void findTrapezeRibbon_tInput1_shouldReturn1(){
        int[] result = Task12.findTrapezeRibbon(INPUT_FOLDER + "/t1.txt");
        int[] expected = new int[]{1};
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void findTrapezeRibbon_tInput2_shouldReturnNull(){
        int[] result = Task12.findTrapezeRibbon(INPUT_FOLDER + "/t2.txt");
        Assert.assertNull(result);
    }
    @Test
    public void findTrapezeRibbon_tInput6_shouldReturn132(){
        int[] result = Task12.findTrapezeRibbon(INPUT_FOLDER + "/t6.txt");
        int[] expected = new int[]{1,3,2};
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void findTrapezeRibbon_tInput7_shouldReturnPathWith10Trapezes(){
        int[] result = Task12.findTrapezeRibbon(INPUT_FOLDER + "/t7.txt");
        int[] expected = new int[]{7,1,4,5,9,3,2,10,8,6};
        Assert.assertArrayEquals(expected, result);
    }
}
