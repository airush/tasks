package tasks;

import org.junit.Assert;
import org.junit.Test;

public class Task1Test {

    @Test
    public void findSingleIntInArrayOfIntPairs_array99millionWithNoSingleElement_shouldReturn0(){
        int[] ints = new int[99_000_000];
        for (int i = 0; i < ints.length/2; i++) {
            ints[i] = i+1;
            ints[ints.length-1] = i+1;
        }
        int expected = 0;
        Assert.assertEquals(expected, Task1.findSingleIntInArrayOfIntPairs(ints));

    }

    @Test
    public void findSingleIntInArrayOfIntPairs_array99millionPlusOne_shouldReturn49500001(){
        int[] ints = new int[99_000_001];
        for (int i = 0; i < ints.length/2; i++) {
            ints[i] = i+1;
            ints[ints.length-1] = i+1;
        }

        int expected = 3;
        ints[49500001] = expected;

        Assert.assertEquals(expected, Task1.findSingleIntInArrayOfIntPairs(ints));
    }

    @Test
    public void findSingleIntInArrayOfIntPairs_smallArrayTest1(){
        int[] ints = new int[]{1,2,1,3,2};
        int expected = 3;

        Assert.assertEquals(expected, Task1.findSingleIntInArrayOfIntPairs(ints));
    }

    @Test
    public void findSingleIntInArrayOfIntPairs_smallArrayTest2(){
        int[] ints = new int[]{3,2,3,3,2};
        int expected = 3;

        Assert.assertEquals(expected, Task1.findSingleIntInArrayOfIntPairs(ints));
    }

    @Test
    public void findSingleIntInArrayOfIntPairs_smallArrayTest3(){
        int[] ints = new int[]{3,3,3,3,3};
        int expected = 3;

        Assert.assertEquals(expected, Task1.findSingleIntInArrayOfIntPairs(ints));
    }

    @Test
    public void findSingleIntInArrayOfIntPairs_smallArrayTest4(){
        int[] ints = new int[]{1,2,2,2};
        int expected = 3;

        Assert.assertEquals(expected, Task1.findSingleIntInArrayOfIntPairs(ints));
    }

    @Test
    public void findSingleIntInArrayOfIntPairs_smallArrayTest5WithNegativeInts(){
        int[] ints = new int[]{-1,2,-1,2,-3};
        int expected = -3;

        Assert.assertEquals(expected, Task1.findSingleIntInArrayOfIntPairs(ints));
    }

}
